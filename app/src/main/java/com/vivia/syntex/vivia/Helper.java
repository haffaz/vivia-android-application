package com.vivia.syntex.vivia;

import com.uber.sdk.android.core.UberSdk;
import com.uber.sdk.rides.client.SessionConfiguration;

public class Helper {

    private static final String CLIENT_ID = "";
    private static final String TOKEN = "";
    private static final String REDIRECT_URI = "";

    public static SessionConfiguration configUber() {
        //uber api configuration
        SessionConfiguration config = new SessionConfiguration.Builder()
                // mandatory
                .setClientId(CLIENT_ID)
                // required for enhanced button features
                .setServerToken(TOKEN)
                // required for implicit grant authentication
                .setRedirectUri(REDIRECT_URI)
                // optional: set sandbox as operating environment
                .setEnvironment(SessionConfiguration.Environment.SANDBOX)
                .build();
        UberSdk.initialize(config);
        return config;
    }

}
