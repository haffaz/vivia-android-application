package com.vivia.syntex.vivia.activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.uber.sdk.android.rides.RideParameters;
import com.uber.sdk.android.rides.RideRequestButton;
import com.uber.sdk.rides.client.ServerTokenSession;
import com.uber.sdk.rides.client.SessionConfiguration;
import com.vivia.syntex.vivia.Helper;
import com.vivia.syntex.vivia.MapsFragment;
import com.vivia.syntex.vivia.R;
import com.vivia.syntex.vivia.Util;
import com.vivia.syntex.vivia.model.Location;
import com.vivia.syntex.vivia.model.Response;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity {

    static final String WEBSOCKET = "Websocket";

    //ui components
    private Button btn_find;
    private TextView tv_state;
    private ProgressBar progressBar;
    private FrameLayout container;
    private LinearLayout layout;

    //uber request button
    private RideRequestButton requestButton;

    //web socket client
    WebSocketClient client;

    SessionConfiguration config;
    //instance of location
    //private Location current;


    //socket url
    private static final String url = "ws://192.168.8.107:1880/android";
    //private static final String url = "ws://192.168.1.38:1880/android";
    //private static final String url = "ws://echo.websocket.org";

    //dummy request
    String message;
    String[] dummyData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initializing ui components
        tv_state = (TextView) findViewById(R.id.tv_state);
        container = (FrameLayout) findViewById(R.id.frameLayout);
        layout = (LinearLayout) findViewById(R.id.layout);
        btn_find = (Button) findViewById(R.id.btn_map);
        //web socket connection
        connectWebSocket();

        //dummy data
        //current = new Location(79.8598505, 6.8652715);
        dummyData= new String[]{
                "b'6.8652715 79.8598505  350' sad false",
                "b'7.2921792 80.6369406  140' sad true",
                "b'6.782244599999999 79.88113309999994  200' sad true",
        };
        //message = dummyData[randomInteger(0,2)] ;
        message = "request" ;

        // get the context by invoking ``getApplicationContext()``, ``getContext()``, ``getBaseContext()`` or ``this`` when in the com.vivia.syntex.vivia.activity class
        requestButton = new RideRequestButton(getApplicationContext());
        //uber api configuration
        config = Helper.configUber();
        //setPickupLocation(requestButton, current, config);


        //SocketConnection con = new SocketConnection();
        //con.execute();

        //displays map
        //displayMap(current);


        //btn_find on click method
        btn_find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    client.send(message);

                    //new MapDialogFragment().show(getSupportFragmentManager(), null);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void displayMap(Location current) {
        MapsFragment mapFragment = new MapsFragment();

        //sending latitude and longitude to map
        Bundle bundle = new Bundle();
        bundle.putDouble("latitude", current.getLatitude());
        bundle.putDouble("longitude", current.getLongitude());
        mapFragment.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(
                R.id.frameLayout, mapFragment
        ).commit();

    }

    private void setPickupLocation(RideRequestButton requestButton, Location current, SessionConfiguration config) {
        RideParameters rideParams = new RideParameters.Builder()
                .setPickupLocation(current.getLatitude(), current.getLongitude(), "Driver", current.getAddress())
                .build();
        requestButton.setRideParameters(rideParams);

        ServerTokenSession session = new ServerTokenSession(config);
        requestButton.setSession(session);

        requestButton.loadRideInformation();
    }

    private void connectWebSocket() {
        URI uri;
        try {
            uri = new URI(url);
            client = new WebSocketClient(uri) {
                @Override
                public void onOpen(ServerHandshake serverHandshake) {
                    Log.i(WEBSOCKET, "Opened");
                    //client.send("Hello from " + Build.MANUFACTURER + " " + Build.MODEL);
                    client.send(message);
                }

                @Override
                public void onMessage(String s) {
                    final String response = s;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(WEBSOCKET, "Message: " + response);
                            responsePopulater(response);
                        }
                    });
                }

                @Override
                public void onClose(int i, String s, boolean b) {
                    Log.i(WEBSOCKET, "Closed " + s);
                }

                @Override
                public void onError(Exception e) {
                    Log.i(WEBSOCKET, "Error " + e.getMessage());
                }
            };
            client.connect();


        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        new Timer().schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            client.send(message);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                30000 // 30 seconds delay
        );

    }

    private void responsePopulater(String response) {
        String[] array = response.split("\\'");
        String[] a1 = array[1].split("\\s+");
        String[] a2 = array[2].split("\\s+");

        try {
            double latitude = Double.parseDouble(a1[1]);
            double longitude = Double.parseDouble(a1[0]);
            final int alcoholLevel = Integer.parseInt(a1[2]);
            String emotion = a2[1];
            final boolean isDrowsy = Boolean.parseBoolean(a2[2]);

            Location current = new Location(longitude, latitude);
            displayMap(current);
            setPickupLocation(requestButton, current, config);
            tv_state.setText("Current State: " + emotion);
            new Timer().schedule(
                    new TimerTask() {
                        @Override
                        public void run() {
                            try {
                                client.send(message);
                                if (isDrowsy) {

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Util.sendNotification("Vivia alert !", "Driver feels sleepy", getApplicationContext());
                                        }
                                    });
                                    //
                                }
                                if (alcoholLevel > 300) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Util.sendNotification("Vivia alert !", "High amount of alcohol", getApplicationContext());

                                        }
                                    });
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    30000 // 30 seconds delay
            );

            Response r = new Response(longitude, latitude, alcoholLevel, emotion, isDrowsy);
            Log.d(WEBSOCKET, longitude + "|" + latitude + "|" +
                    alcoholLevel + "|" + emotion + "|" + isDrowsy);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showAlertBox(String title, String content) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(content);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

 /*   public void updateLocation(android.location.Location location) {
        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(
                location.getLatitude(), location.getLongitude()
        )).title("Driver Location").snippet("Map"));
    }*/

    public void sendNotification(String title, String content) {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this);

        //Create the intent that’ll fire when the user taps the notification//

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        mBuilder.setContentIntent(pendingIntent);

        mBuilder.setSmallIcon(R.drawable.googleg_disabled_color_18);
        mBuilder.setContentTitle(title);
        mBuilder.setContentText(content);

        NotificationManager mNotificationManager =

                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(001, mBuilder.build());
    }


    private void setCurrent(double longitude, double latitude) {
        Location current = new Location(longitude, latitude);
    }

    private int randomInteger(int min, int max) {
        Random random = new Random();
        return random.nextInt(max + 1 - min) + min;
    }
    public class SocketConnection extends AsyncTask<Void, Void, Location> {
        //Location current;

        @Override
        protected Location doInBackground(Void... arg0) {
            URI uri;
            try {
                uri = new URI(url);
                client = new WebSocketClient(uri) {
                    @Override
                    public void onOpen(ServerHandshake serverHandshake) {
                        Log.i(WEBSOCKET, "Opened");
                        client.send("request: " + Build.MANUFACTURER + " " + Build.MODEL);
                    }

                    @Override
                    public void onMessage(String s) {
                        final String message = s;

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                responsePopulater(message);
                                //displayMap(current);
                            }
                        });
                    }

                    @Override
                    public void onClose(int i, String s, boolean b) {
                        Log.i(WEBSOCKET, "Closed " + s);
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.i(WEBSOCKET, "Error " + e.getMessage());
                    }
                };
                client.connect();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Location current) {
            super.onPostExecute(current);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //uber api configuration
                    //SessionConfiguration config = Helper.configUber();
                    //setPickupLocation(requestButton, SocketConnection.this.current, config);

                    //displayMap(SocketConnection.this.current);
                }
            });
        }
    }
}
