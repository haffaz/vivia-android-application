package com.vivia.syntex.vivia;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.uber.sdk.android.rides.RideParameters;
import com.uber.sdk.android.rides.RideRequestButton;
import com.uber.sdk.rides.client.ServerTokenSession;
import com.uber.sdk.rides.client.SessionConfiguration;

public class MapsFragment extends Fragment implements OnMapReadyCallback {

    private static final String TAG = "RRR";

    private GoogleMap mGoogleMap;
    private MapView mMapView;
    private FloatingActionButton refresh;
    private View mView;

    //uber request button
    private RideRequestButton requestButton;
    SessionConfiguration config;


    private double latitude;
    private double longitude;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        mView = inflater.inflate(R.layout.fragment_map, container, false);

        latitude = getArguments().getDouble("latitude");
        longitude = getArguments().getDouble("longitude");

        //<------------------------- UBER API ------------------------->//
        requestButton = new RideRequestButton(mView.getContext());
        //uber api configuration
        config = Helper.configUber();

        RideParameters rideParams = new RideParameters.Builder()
                .setPickupLocation(latitude, longitude, "Driver", "Address")
                .build();
        requestButton.setRideParameters(rideParams);
        ServerTokenSession session = new ServerTokenSession(config);
        requestButton.setSession(session);
        requestButton.loadRideInformation();
        //<------------------------- -------- ------------------------->//


        refresh = (FloatingActionButton) mView.findViewById(R.id.btn_refresh);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMapView.invalidate();
                Toast.makeText(getActivity(), "invalidate", Toast.LENGTH_SHORT).show();
            }
        });


        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated: ");
        super.onViewCreated(view, savedInstanceState);

        mMapView = (MapView) mView.findViewById(R.id.mapView);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getActivity());

        mGoogleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.addMarker(new MarkerOptions().position(new LatLng(
                latitude, longitude
        )).title("Driver Location").snippet("Map"));

        CameraPosition pos = CameraPosition.builder().target(new LatLng(
                latitude, longitude
        )).zoom(16).bearing(0).tilt(45).build();

        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(pos));
    }
}