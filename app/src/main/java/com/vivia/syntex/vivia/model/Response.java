package com.vivia.syntex.vivia.model;

public class Response {
    private double latitude;
    private double longitude;
    private int alchoholLevel;
    private String emotion;
    private boolean isDrowsy;

    public Response(double latitude, double longitude, int alchoholLevel, String emotion, boolean isDrowsy) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.alchoholLevel = alchoholLevel;
        this.emotion = emotion;
        this.isDrowsy = isDrowsy;
    }

    public Response() {

    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getAlchoholLevel() {
        return alchoholLevel;
    }

    public void setAlchoholLevel(int alchoholLevel) {
        this.alchoholLevel = alchoholLevel;
    }

    public String getEmotion() {
        return emotion;
    }

    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }

    public boolean isDrowsy() {
        return isDrowsy;
    }

    public void setDrowsy(boolean drowsy) {
        isDrowsy = drowsy;
    }
}
